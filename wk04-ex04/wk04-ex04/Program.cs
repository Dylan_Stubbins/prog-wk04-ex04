﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wk04_ex04
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter in a whole number");

            try
            {
                var input = int.Parse(Console.ReadLine());
                Console.WriteLine($"You entered {input}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            
        }
    }
}
